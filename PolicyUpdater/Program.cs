﻿using Amazon.S3;
using Amazon.S3.Util;
using Coscine.Database.Models;
using Coscine.ResourceTypes;
using Coscine.ResourceTypes.RdsS3;
using Coscine.ResourceTypes.RdsS3Worm;

var resources = new ResourceModel();
var resourceOptions = new ResourceTypeModel();
var success = new List<string>();
var failed = new List<string>();
var nonExistend = new List<string>();

var rdss3 = resourceOptions.GetAllWhere(x => x.Type == "rdss3").Select(x => x.Id).ToList();
var rdss3worm = resourceOptions.GetAllWhere(x => x.Type == "rdss3worm").Select(x => x.Id).ToList();

var toUpdate = resources.GetAllWhere(r => rdss3.Contains(r.TypeId) || rdss3worm.Contains(r.TypeId));

Console.WriteLine($"Will update {toUpdate.Count()} resources...");

foreach (var resource in toUpdate)
{
    try
    {
        Console.WriteLine($"Fetch definition for {resource.Id}");
        var resourceTypeDefinition = ResourceTypeFactory.Instance.GetResourceType(resource);

        var resourceTypeRds = resourceTypeDefinition as RdsS3ResourceType;
        AmazonS3Client? client = null;

        if (resourceTypeRds is not null)
        {
            client = new AmazonS3Client(resourceTypeRds?.RdsS3ResourceTypeConfiguration.AccessKey, resourceTypeRds?.RdsS3ResourceTypeConfiguration.SecretKey, new AmazonS3Config
            {
                ServiceURL = resourceTypeRds?.RdsS3ResourceTypeConfiguration.Endpoint,
                ForcePathStyle = true
            });
        }

        var resourceTypeRdsWorm = resourceTypeDefinition as RdsS3WormResourceType;
        if (resourceTypeRdsWorm is not null)
        {
            client = new AmazonS3Client(resourceTypeRdsWorm?.RdsS3WormResourceTypeConfiguration.AccessKey, resourceTypeRdsWorm?.RdsS3WormResourceTypeConfiguration.SecretKey, new AmazonS3Config
            {
                ServiceURL = resourceTypeRdsWorm?.RdsS3WormResourceTypeConfiguration.Endpoint,
                ForcePathStyle = true
            });
        }

        if (client == null)
        {
            throw new Exception("Neither s3 nor s3worm resource!");
        }

        var exists = false;

        try
        {
            exists = AmazonS3Util.DoesS3BucketExistV2Async(client, resource.Id.ToString()).Result;
        }
        catch
        {
        }

        if (exists)
        {
            Console.WriteLine($"Update policy for {resource.Id}");
            resourceTypeDefinition.SetResourceReadonly(resource.Id.ToString(), resource.Archived == "1");
            Console.WriteLine($"Successfully updated {resource.Id}");
            success.Add(resource.Id.ToString());
        }
        else
        {
            Console.WriteLine($"Bucket for {resource.Id} does not exist");
            nonExistend.Add(resource.Id.ToString());
        }
    }
    catch (Exception e)
    {
        Console.WriteLine($"Failed to updated {resource.Id}");
        failed.Add(resource.Id.ToString());
        Console.WriteLine(e);
    }
}

Console.WriteLine($"Successfully updated {success.Count} resources");

if (success.Count > 0)
{
    Console.WriteLine($"{success.Count} resources were updated");
    foreach (var suc in success)
    {
        Console.WriteLine($"{suc} was updated");
    }
}

if (nonExistend.Count > 0)
{
    Console.WriteLine($"{nonExistend.Count} resources had not bucket attached.");
    foreach (var exist in nonExistend)
    {
        Console.WriteLine($"{exist} has no bucket.");
    }
}

if (failed.Count > 0)
{
    Console.WriteLine($"Failed to update {failed.Count} resources. Check output for exceptions.");
    foreach (var fail in failed)
    {
        Console.WriteLine($"Failed update for resource {fail}.");
    }
}

Console.WriteLine("Done");